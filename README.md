# Brain idcard web

@written by 황원  

ibk 위변조 판별에서 사용한 demo web code입니다.


## Engine
ibk 위변조 판별 모델은 아래 bitbucket 주소에서 확인하실 수 있습니다.
```angular2
https://pms.maum.ai/bitbucket/projects/BRAIN/repos/brain_idcard_auth/browse?at=refs%2Fheads%2Fgrpc
```
or
```angular2
docker pull docker.maum.ai:443/brain/ibk_authentication:v0-engine
```

## Web
```angular2
docker pull docker.maum.ai:443/brain/ibk_authentication:v0-web
```
## Web run command

```angular2
docker run -it --ipc=host -p 4575:4575 --name ibk_web_test docker.maum.ai:443/brain/ibk_authentication:v0-web
```

## Issue
ssh로 붙어야 페이지가 정상적으로 표시됩니다.
```angular2
ex) ssh -NfL localhost:4575:localhost:4575 USERID@WEB_SERVER_IP
```

## 담당자
@ JinwooKim (acasia@mindslab.ai)