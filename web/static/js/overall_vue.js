var forurl = "http://127.0.0.1:4575"

var vm = new Vue({
            el: '#onscreen',
            data: {
                nowUploadStatus:0,
                nowSample:1,
                nowImgName:"",
                nowloading:0,
                nowOriginal:0,
                nowPaper:0,
                nowMonitor:0,
                forurl:"http://127.0.0.1:4575",
                overallLabel:"",
            }
        })
function uploadBtnClick(){
    document.getElementById("ex_file").click();

  }
function uploadBtnClick2(){
    document.getElementById("fileuploader").click();
}
function AfterUpload(){
    document.getElementById("submitbut").click();
}



function uploadFile(){
    var fileInput = document.getElementById("ex_file");
    if(fileInput.files.length<1){
        alert("No File");
        return 0;
    }
    let url = forurl + "/upload";
    let formData = new FormData();
        formData.append("file" , fileInput.files[0]);
        let xhr = new XMLHttpRequest();
        xhr.open("POST" , url , false);
        xhr.onload = function () {
            vm.nowImgName = (xhr.responseText)
            vm.nowUploadStatus = 1
            vm.nowSample = 0
        }
        xhr.send(formData);
}

function processFile(){
    let url = forurl + "/process";
    vm.nowloading = 1;
    vm.nowUploadStatus = 2;
    if(vm.nowSample==1){
        vm.nowImgName="sample_monitor"
    }
    let formData = new FormData();
    if(vm.nowSample==0){formData.append("filename" , vm.nowImgName);}
    else{formData.append("filename" , "sample_monitor");}
    let xhr = new XMLHttpRequest();
    xhr.open("POST" , url , false);
    xhr.onload = function () {
        vm.nowloading = 0;
        vm.nowUploadStatus = 3;

        let nowresText = (xhr.responseText).split("/")
        if (nowresText=='sample'){
            vm.nowPaper = 0
            vm.nowMonitor = 12
            vm.overallLabel="Monitor"
        }else{
            vm.nowOriginal = nowresText[0]
             vm.nowPaper = nowresText[2]
            vm.nowMonitor = nowresText[1]
            vm.overallLabel = ["Original", "Monitor", "Paper"][nowresText[3]]
        }
    }
    xhr.send(formData);
}

function resetPage(){
    vm.nowUploadStatus=0;
    vm.nowSample=1
    vm.nowImgName=""
    vm.nowloading=0
}