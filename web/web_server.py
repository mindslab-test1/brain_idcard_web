import os
import grpc
import uuid
import cv2
import atexit

from flask import Flask, render_template, request, send_file
from flask_cors import CORS, cross_origin
from werkzeug.utils import secure_filename
from apscheduler.schedulers.background import BackgroundScheduler

from id_card_authentication_proto_pb2 import IdCard
from id_card_authentication_proto_pb2_grpc import IdCardAuthenticationStub


class MyFlask(Flask):
    jinja_options = Flask.jinja_options.copy()
    jinja_options.update(dict(
        block_start_string='{%',
        block_end_string='%}',
        variable_start_string='((',
        variable_end_string='))',
        comment_start_string='{#',
        comment_end_string='#}',
    ))


app = MyFlask(__name__, static_folder="./static")
CORS(app)
app.config['UPLOAD_FOLDER'] = "tmp/uploaded"  # WEB->UPLOAD_FOLDER
app.config['MAKE_FOLDER'] = "tmp/make"


def root_dir():  # pragma: no cover
    return os.path.abspath(os.path.dirname(__file__))


def get_file(filename):  # pragma: no cover
    try:
        src = os.path.join(root_dir(), filename)
        return open(src).read()
    except IOError as exc:
        return str(exc)


@app.route('/')
def index():
    return render_template('login.html')


@app.route('/login', methods=['GET'])
def login_index():
    return render_template('login.html')


@app.route('/login', methods=['POST'])
def login():
    try:
        str_id = request.form['id']
        str_pw = request.form['pw']

        if ("client" in str_id):
            if ("test" in str_pw):
                print("Login Success")
                return render_template('return.html')
        return render_template('login.html')
    except:
        return render_template('login.html')


@app.route('/upload', methods=['GET', 'POST'])
def upload():
    file = request.files['file']
    filename = str(uuid.uuid4()) + secure_filename(file.filename)
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return filename


@app.route('/img/<img_id>', methods=['GET'])
def get_image(img_id):
    if (img_id == 'sample_monitor'):
        return send_file("sample_monitor.jpg")
    else:
        return send_file("../tmp/uploaded/" + img_id)


@app.route('/maked/<img_id>', methods=['GET'])
def get_maked_image(img_id):
    if (img_id == 'sample_monitor'):
        return send_file("maked_sample_monitor.jpg")
    else:
        return send_file("../tmp/make/" + img_id)


@app.route('/js/<js>', methods=['GET'])
def get_js(js):
    return send_file("static/js/" + js)


@app.route('/process', methods=['POST'])
def overall_process():
    filename = request.form['filename']
    if (filename == "sample_monitor"):
        return "sample"
    after = GRPC_check_id_card(filename)
    return after


CHUNK_SIZE = 1024 * 1024  # 1MB


def get_file_chunks(filename):
    print("getFileChunks " + filename)
    with open(filename, 'rb') as f:
        while True:
            piece = f.read(CHUNK_SIZE)
            if len(piece) == 0:
                return
            yield IdCard(data=piece)


address = '182.162.19.29:11000'  # model address:port
channel = grpc.insecure_channel(address)
clientstub = IdCardAuthenticationStub(channel)


def GRPC_check_id_card(filename_in_upload_folder):
    chunk_generator = get_file_chunks(app.config['UPLOAD_FOLDER'] + "/" + filename_in_upload_folder)
    response = clientstub.Determinate(chunk_generator)
    nowprint = list()
    # print(response)
    overall_label = str(response.label)
    for T in response.patches:
        label = T.label
        width = T.width
        height = T.height
        left = T.left
        top = T.top
        nowprint.append([label, width, height, left, top])
    ori_image = cv2.imread(app.config['UPLOAD_FOLDER'] + "/" + filename_in_upload_folder)
    cvimage = cv2.imread(app.config['UPLOAD_FOLDER'] + "/" + filename_in_upload_folder)

    int_cat = [0, 0, 0]

    bgr_point = [(0, 0, 0, 1), (0, 0, 255, 0.5), (0, 255, 255, 0.5)]
    for L in nowprint:
        cvimage = cv2.rectangle(cvimage, (L[3], L[4]), ((L[3] + L[1]), (L[4] + L[2])), bgr_point[L[0]], -1)
        int_cat[L[0]] = int_cat[L[0]] + 1

    cvimage = cv2.addWeighted(ori_image, 0.5, cvimage, 0.5, 0)

    for L in nowprint:
        cvimage = cv2.rectangle(cvimage, (L[3], L[4]), ((L[3] + L[1]), (L[4] + L[2])), (0, 0, 0), 2)

    cv2.imwrite(app.config['MAKE_FOLDER'] + "/" + filename_in_upload_folder, cvimage)

    return str(int_cat[0]) + "/" + str(int_cat[1]) + "/" + str(int_cat[2]) + "/" + overall_label


def print_date_time():
    remove_all_file_in_folder("tmp/uploaded")
    remove_all_file_in_folder("tmp/make")


def remove_all_file_in_folder(foldername):
    if os.path.exists(foldername):
        for file in os.scandir(foldername):
            os.remove(file.path)


def directory_validation():
    if not (os.path.isdir('tmp')):
        os.makedirs(os.path.join('tmp'))
    if not (os.path.isdir('tmp/uploaded')):
        os.makedirs(os.path.join('tmp/uploaded'))
    if not (os.path.isdir('tmp/make')):
        os.makedirs(os.path.join('tmp/make'))


def static_file_reset(ip, port):
    f = open("web/ori_file/overall_vue.js", 'r')
    vuedata = str(f.read())
    vuedata = vuedata.replace("<<IP>>", str(ip))
    vuedata = vuedata.replace("<<PORT>>", str(port))
    f.close()
    f = open("web/static/js/overall_vue.js", 'w')
    f.write(vuedata)
    f.close()

    f = open("web/ori_file/return.html", 'r')
    htmldata = str(f.read())
    htmldata = htmldata.replace("<<IP>>", str(ip))
    htmldata = htmldata.replace("<<PORT>>", str(port))
    f.close()
    f = open("web/templates/return.html", 'w')
    f.write(htmldata)
    f.close()


if __name__ == "__main__":
    directory_validation()
    static_file_reset("127.0.0.1", 4575)

    scheduler = BackgroundScheduler()
    scheduler.add_job(func=print_date_time, trigger="interval", minutes=59)
    scheduler.start()

    atexit.register(lambda: scheduler.shutdown())

    app.run(host="0.0.0.0", port=4575)
    # GRPC_check_id_card("b9a1117a-bf99-41c1-856f-88f896c2b697original.jpg")
