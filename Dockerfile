FROM docker.maum.ai:443/brain/vision:cu101-torch160

COPY . /workspace
WORKDIR /workspace

EXPOSE 11000
EXPOSE 4575

RUN apt-get update
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ENTRYPOINT python web/web_server.py
